﻿using JwtBearer.Model;
using JwtBearer.Repositories;
using JwtBearer.Services;
using Microsoft.AspNetCore.Mvc;

namespace JwtBearer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<dynamic>> AuthenticateAsync([FromBody] User model)
        {
            var user = UserRepository.Get(model.Username, model.Password);
            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos!"});

                var token = TokenService.GenerateToken(user);
                user.Password = "";
                return new { user = user, token = token };
        }
    }
}
