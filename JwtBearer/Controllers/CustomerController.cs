﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JwtBearer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        [HttpGet]
        [Route("anonymous")]
        [AllowAnonymous]
        public string Anonymous()
        {
            return "Anônimo";
        }

        [HttpGet]
        [Route("authenticated")]
        [Authorize]
        public string Authenticathed()
        {
            return String.Format($"Autenticado - {User.Identity.Name}");
        }

        [HttpGet]
        [Route("employee")]
        [Authorize(Roles = "employee,manager")]
        public string Employe() 
        {
            return "Funcionário";
        }
        [HttpGet]
        [Route("manager")]
        [Authorize(Roles = "manager")]
        public string Manager()
        {
            return "Gerente";
        }
    }
}
